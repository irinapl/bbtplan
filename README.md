# Theater performance plan

# bbtplan

Prøveplan for BBT

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification and deploy
npm run build

# For deploy you will need firebase tools
npm install -g firebase-tools

# Deploy app to production after build
firebase deploy

#Application is available at
https://bbt-plan.firebaseapp.com/

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
