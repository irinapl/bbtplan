import firebase from 'firebase/app'
import 'firebase/database'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAsVGKfyxLV5N8jZodsUKZAsMVhyEm0_rA',
  authDomain: 'bbt-plan.firebaseapp.com',
  databaseURL: 'https://bbt-plan.firebaseio.com',
  projectId: 'bbt-plan',
  storageBucket: 'bbt-plan.appspot.com',
  messagingSenderId: '908937125994'
}
firebase.initializeApp(config)

const performancesRef = firebase.database().ref('performances')

export default {
  performances: performancesRef,

  async getPerformance (id, callback) {
    performancesRef.child(id).once('value')
      .then(function (snapshot) {
        callback(snapshot.val())
      })
  },

  getActivePerformance (callback) {
    performancesRef.orderByChild('created_at').limitToFirst(1).on('child_added', function (snapshot) {
      callback(snapshot.key, snapshot.val())
    })
  },

  async createPerformance (performance) {
    performance.created_at = -1 * new Date().getTime()
    await performancesRef.push().set(performance)
  },

  async updatePerformance (id, performance) {
    performance.updated_at = -1 * new Date().getTime()
    await performancesRef.child(id).set(performance)
  },

  async deletePerformance (key) {
    await performancesRef.child(key).remove()
  },

  addWeekPlan (id, plansByWeek) {
    let changes = {}
    Object.keys(plansByWeek).forEach(function (week) {
      changes[`performances/${id}/plans/${week}`] = plansByWeek[week]
    })
    return firebase.database().ref().update(changes)
  }
}
