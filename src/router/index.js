import Vue from 'vue'
import Router from 'vue-router'
import Auth from '@/Auth'
import MyRoles from '@/components/MyRoles'
import Dashboard from '@/components/Dashboard'
import Plan from '@/components/admin/Plan'
import Performances from '@/components/admin/Performances'
import PerformanceForm from '@/components/admin/PerformanceForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/myroles',
      name: 'MyRoles',
      component: MyRoles
    },
    {
      path: '/admin/forestillinger',
      name: 'performances',
      component: Performances
    },
    {
      path: '/admin/forestillinger/ny',
      name: 'new-performance',
      component: PerformanceForm
    },
    {
      path: '/admin/forestillinger/:id',
      name: 'edit-performance',
      component: PerformanceForm
    },
    {
      path: '/admin/plan',
      name: 'Plan',
      component: Plan
    }
  ]
})
