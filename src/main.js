// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import Vuefire from 'vuefire'

import db from './services/firebase'
// import firebase from 'firebase'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Vuefire)

Vue.config.errorHandler = function (err, vm, info) {
  console.error(err, vm, info)
  // handle error
  // `info` is a Vue-specific error info, e.g. which lifecycle hook
  // the error was found in. Only available in 2.2.0+
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  data: {
    userId: null
  },
  router,
  template: '<App/>',
  components: { App },
  firebase: {
    performances: db.performances.orderByChild('created_at')
  }
  /* created () {
    firebase.auth().onAuthStateChanged((user) => {
      console.log('onAuthStateChanged: ', user)
      if (user) {
        this.$router.push('/')
      } else {
        console.log('go to login page: ')
        this.$router.push('/auth')
      }
    })
  } */
  /* created () {
    firebase.auth().signInAnonymously().catch(function (error) {
      console.log('Could not login: ', error)
    })
    firebase.auth().onAuthStateChanged(function (user) {
      this.userId = user.uid
    })
  } */
})
