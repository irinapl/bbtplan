'use strict'
import readXlsxFile from 'read-excel-file'
import moment from 'moment'

moment.locale('nb')

module.exports = {
  readXlsxFile(file, { schema }).then((data) => {
    let lastDate
    let lastTrainingType
    let lastExecize
    let lastGroup
    data.forEach(function (row) {
      const dateColumn = row[0]
      if (dateColumn && !dateColumn.startsWith('UKE')) {
        lastDate = dateColumn.substring(4) || lastDate
        let trainingDate = moment(lastDate, 'DD. MMM')
        const fromTime = row[1]
        const toTime = row[2]
        lastTrainingType = row[3] || lastTrainingType
        lastExecize = row[4] || lastExecize
        const roles = row[5] || 'ALLE'
        lastGroup = row[6] || lastGroup
        selv.plan.push({
          date: moment(trainingDate).format('YYYY-MM-DD'),
          fromTime: moment(fromTime, 'HH:mm').format('HH:mm'),
          toTime,
          trainingType: lastTrainingType,
          execize: lastExecize,
          roles: roles.split(),
          group: lastGroup
        })
      }
    })
  })
  }
}
