'use strict'
import moment from 'moment'

moment.locale('nb')

export const groupBy = (xs, f) => {
  return xs.reduce((r, v, i, a, k = f(v)) => {
    return ((r[k] || (r[k] = [])).push(v), r)
  }, {})
}
